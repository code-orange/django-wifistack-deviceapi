from django.urls import path, include
from tastypie.api import Api

from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import (
    ApiAuthCustomerApiKeyResource,
    ApiAuthCustomerResource,
)
from django_wifistack_deviceapi.django_wifistack_deviceapi.api.resources import *
from django_wifistack_deviceapi.django_wifistack_deviceapi.views import *

v2_api = Api(api_name="v2")

# API GENERAL
v2_api.register(ApiAuthCustomerApiKeyResource())
v2_api.register(ApiAuthCustomerResource())

# API WIFI
v2_api.register(WirelessNodeConfigResource())
v2_api.register(GeoJsonDomainResource())

# API ADMINISTRATION
v2_api.register(WirelessNodeAdminResource())
v2_api.register(DomainResource())

urlpatterns = [
    path("", include(v2_api.urls)),
    path(
        "data.geojson",
        WirelessNodesModelLayer.as_view(
            model=CmdbHost,
            properties=(
                "title",
                "is_online",
            ),
            geometry_field="geoloc",
        ),
    ),
    path("heartbeat/<str:node_id>", heartbeat),
    path("local-exec/<str:node_id>", local_exec),
    path("ipconf/<str:node_id>", ipconf),
]
