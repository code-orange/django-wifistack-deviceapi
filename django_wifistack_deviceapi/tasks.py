from datetime import datetime

import requests
from celery import shared_task
from dateutil.parser import parse as datetime_parse

from django_cdstack_models.django_cdstack_models.models import CmdbHost, CmdbVarsHost
from django_wifistack_deviceapi.django_wifistack_deviceapi.models import (
    WifiStackMeshviewerUrl,
    WifiStackCommunityInstance,
)
from django_wifistack_deviceapi.django_wifistack_deviceapi.views import heartbeat


def get_meshviewer_data():
    urls = WifiStackMeshviewerUrl.objects.all()

    node_data = dict()

    for nodelist_source in urls:
        nodelist = requests.get(nodelist_source.url + "nodelist.json")
        meshviewer = requests.get(nodelist_source.url + "meshviewer.json")

        if nodelist.status_code != 200 and meshviewer.status_code != 200:
            continue

        nodelist_data = nodelist.json()
        meshviewer_data = meshviewer.json()

        if "nodes" not in nodelist_data:
            continue

        if nodelist_data["nodes"] is None:
            continue

        for node_row in nodelist_data["nodes"]:
            if node_row["id"] in node_data:
                lastcontact_old = datetime_parse(
                    node_data[node_row["id"]]["status"]["lastcontact"]
                )
                lastcontact_new = datetime_parse(node_row["status"]["lastcontact"])

                if lastcontact_new > lastcontact_old:
                    node_data[node_row["id"]] = node_row
            else:
                node_data[node_row["id"]] = node_row

            for node_meshviewer in meshviewer_data["nodes"]:
                if node_meshviewer["node_id"] == node_row["id"]:
                    node_data[node_row["id"]] = {
                        **node_data[node_row["id"]],
                        **node_meshviewer,
                    }

    return node_data


@shared_task(name="process_meshviewer_data")
def process_meshviewer_data():
    for node_id, node in get_meshviewer_data().items():
        try:
            cmdb_host = CmdbHost.objects.get(
                hostname="dc-" + node["id"] + ".nodes.managed-infra.com"
            )
        except CmdbHost.DoesNotExist:
            continue

        lastcontact = datetime_parse(node["status"]["lastcontact"])

        if lastcontact.timestamp() > cmdb_host.host_last_fetch.timestamp():
            heartbeat(None, node["id"])
            cmdb_host.host_last_fetch = datetime.now()
            cmdb_host.save()

    return


@shared_task(name="sync_meshviewer_domain")
def sync_meshviewer_domain():
    for node_id, node in get_meshviewer_data().items():
        try:
            cmdb_host = CmdbHost.objects.get(
                hostname="dc-" + node["id"] + ".nodes.managed-infra.com"
            )
        except CmdbHost.DoesNotExist:
            continue

        try:
            wireless_node_meshviewer_domain = cmdb_host.cmdbvarshost_set.get(
                name="wireless_node_meshviewer_domain"
            )
        except CmdbVarsHost.DoesNotExist:
            wireless_node_meshviewer_domain = CmdbVarsHost(
                host_rel=cmdb_host,
                name="wireless_node_meshviewer_domain",
                value="default",
            )
            wireless_node_meshviewer_domain.save()

        if wireless_node_meshviewer_domain.value != node["domain"]:
            wireless_node_meshviewer_domain.value = node["domain"]
            wireless_node_meshviewer_domain.save()

        try:
            wireless_node_domain = cmdb_host.cmdbvarshost_set.get(
                name="wireless_node_domain"
            )
        except CmdbVarsHost.DoesNotExist:
            wireless_node_domain = CmdbVarsHost(
                host_rel=cmdb_host,
                name="wireless_node_domain",
                value=wireless_node_meshviewer_domain.value,
            )
            wireless_node_domain.save()

        if wireless_node_domain.value == "" or wireless_node_domain.value is None:
            wireless_node_domain.value = wireless_node_meshviewer_domain.value
            wireless_node_domain.save()

    return


@shared_task(name="auto_assign_instance_for_new_nodes")
def auto_assign_instance_for_new_nodes():
    new_nodes = CmdbHost.objects.filter(
        hostname__endswith=".nodes.managed-infra.com", inst_rel_id=1
    )

    for new_node in new_nodes:
        try:
            domain = new_node.cmdbvarshost_set.get(name="wireless_node_domain")
        except CmdbVarsHost.DoesNotExist:
            continue

        try:
            instance = WifiStackCommunityInstance.objects.get(domain=domain.value)
        except WifiStackCommunityInstance.DoesNotExist:
            continue

        new_node.inst_rel_id = instance.instance_id
        new_node.save()

    return
