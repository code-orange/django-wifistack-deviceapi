from urllib.parse import urljoin

import requests
from crequest.middleware import CrequestMiddleware
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from djgeojson.views import GeoJSONLayerView
from requests import ConnectionError

from django_cdstack_deploy.django_cdstack_deploy.func import get_host_vars
from django_cdstack_models.django_cdstack_models.models import *


class WirelessNodesModelLayer(GeoJSONLayerView):
    def get_queryset(self):
        current_request = CrequestMiddleware.get_request()
        domain = current_request.GET.get("domain", None)

        context = CmdbHost.objects.filter(
            hostname__endswith=".nodes.managed-infra.com"
        ).filter(host_last_fetch__gte=datetime.now() - timedelta(days=7))

        if domain:
            context = context.filter(
                cmdbvarshost__name="wireless_node_domain", cmdbvarshost__value=domain
            )

        return context


def local_exec(request, node_id):
    default_hostname = "dc-" + node_id + ".nodes.managed-infra.com"

    try:
        heartbeat(request, node_id)
    except:
        pass

    try:
        host = CmdbHost.objects.get(hostname=default_hostname)
    except CmdbHost.DoesNotExist:
        return HttpResponse("# INVALID NODE")

    template_opts = dict(get_host_vars(host))

    if "local_exec" not in template_opts:
        return HttpResponse("# NO LOCAL EXEC SET UP")

    return HttpResponse(template_opts["local_exec"])


@csrf_exempt
def heartbeat(request, node_id):
    default_hostname = "dc-" + node_id + ".nodes.managed-infra.com"

    try:
        host = CmdbHost.objects.get(hostname=default_hostname)
    except CmdbHost.DoesNotExist:
        return HttpResponse("INVALID NODE")

    # pseudo_hostname example: h21-webserver.example.com
    object_name_str = str("h" + str(host.id) + "-" + host.hostname).lower()

    session = requests.Session()
    session.headers = {
        "Accept": "application/json",
    }

    session.auth = (settings.ICINGA_MAIN_API_USER, settings.ICINGA_MAIN_API_PASSWD)

    request_url = urljoin(
        settings.ICINGA_MAIN_API_URL, "v1/actions/process-check-result"
    )

    payload = dict()
    payload["type"] = "Host"
    payload["filter"] = 'host.name=="' + object_name_str + '"'
    payload["exit_status"] = 0
    payload["plugin_output"] = "Node sent heartbeat."

    # create arguments for the request
    request_args = {
        "url": request_url,
        "json": payload,
        "verify": False,
        "timeout": 5,
    }

    # do the request
    try:
        response = session.post(**request_args)
    except ConnectionError:
        # Ignore connection errors and wait for next run
        return

    return HttpResponse(response.content)


@csrf_exempt
def ipconf(request, node_id):
    default_hostname = "dc-" + node_id + ".nodes.managed-infra.com"

    try:
        heartbeat(request, node_id)
    except:
        pass

    try:
        host = CmdbHost.objects.get(hostname=default_hostname)
    except CmdbHost.DoesNotExist:
        return HttpResponse("# INVALID NODE")

    try:
        ip_config = host.cmdbvarshost_set.get(name="info_dump_ip_config")
    except CmdbVarsHost.DoesNotExist:
        ip_config = CmdbVarsHost(host_rel=host, name="info_dump_ip_config")

    try:
        info_dump_enabled = host.cmdbvarshost_set.get(name="info_dump_enabled")
    except CmdbVarsHost.DoesNotExist:
        info_dump_enabled = CmdbVarsHost(
            host_rel=host, name="info_dump_enabled", value="true"
        )

    if info_dump_enabled.value == "true":
        ip_config.value = request.body.decode("utf-8")
        ip_config.save()

        info_dump_enabled.value = "false"

    info_dump_enabled.save()

    return HttpResponse("OK")
