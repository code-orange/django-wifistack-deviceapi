from django.contrib.gis.geos import Point
from django.forms.models import model_to_dict
from tastypie.exceptions import ImmediateHttpResponse, NotFound
from tastypie.http import HttpConflict
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_cdstack_deploy.django_cdstack_deploy.authentification import *
from django_cdstack_deploy.django_cdstack_deploy.func import get_host_vars
from django_cdstack_models.django_cdstack_models.models import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)
from django_wifistack_deviceapi.django_wifistack_deviceapi.models import *
from django_wifistack_deviceapi.django_wifistack_deviceapi.views import heartbeat
from django_wifistack_main.django_wifistack_main.access import (
    domain_aliases_for_customer,
)
from django_wifistack_main.django_wifistack_main.queryset import (
    nodes_assigned_to_domain_aliases,
)
from django_wifistack_models.django_wifistack_models.models import WifiStackCustomerSite


class WirelessNodeConfigResource(Resource):
    class Meta:
        queryset = CmdbHost.objects.filter(hostname__endswith="nodes.managed-infra.com")
        resource_name = "wirelessnode"
        always_return_data = True
        allowed_methods = ["get", "post"]
        limit = 0
        max_limit = 0
        serializer = Serializer()
        authentication = AnonymousGetAuthentication(
            ApiAuthCustomerAuthentication(),
        )
        public_vars = (
            "node_hostname_fqdn",
            "wireless_node_title",
            "wireless_node_domain",
            "wireless_node_geo_lat",
            "wireless_node_geo_long",
            "wireless_node_lldp_enabled",
            "wireless_node_snmp_enabled",
            "wireless_node_updater_enabled",
            "wireless_node_updater_branch",
            "wireless_node_mesh_on_lan_enabled",
            "wireless_node_lan_bridge",
            "wireless_node_captive_portal_enabled",
            "monitoring_syslog_receiver_01_ip",
            "monitoring_syslog_receiver_01_port",
            "monitoring_snmp_trap_receiver_01_ip",
            "monitoring_snmp_trap_receiver_01_port",
            "wireless_node_traffic_shaping_enabled",
            "wireless_node_traffic_shaping_egress_kbps",
            "wireless_node_traffic_shaping_ingress_kbps",
            "info_dump_ip_config",
            "wifi_networks",
        )
        editable_vars = (
            "wireless_node_title",
            "wireless_node_geo_lat",
            "wireless_node_geo_long",
            "wireless_node_lldp_enabled",
            "wireless_node_snmp_enabled",
            "wireless_node_mesh_on_lan_enabled",
            "wireless_node_lan_bridge",
        )

    def deserialize(self, request, data, format=None):
        if request.META.get("HTTP_USER_AGENT", "unknown") == "uclient-fetch-json":
            request.META["CONTENT_TYPE"] = "application/json"

        return super().deserialize(request, data, format)

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        default_hostname = "dc-" + kwargs["pk"] + ".nodes.managed-infra.com"

        try:
            host = CmdbHost.objects.get(hostname=default_hostname)
        except CmdbHost.DoesNotExist:
            host = CmdbHost(
                hostname=default_hostname,
                enabled=1,
                inst_rel_id=1,
                os=CmdbOsVersion.objects.get(name="OpenWRT"),
            )
            host.save(force_insert=True)

        try:
            wireless_node_meshviewer_domain = host.cmdbvarshost_set.get(
                name="wireless_node_meshviewer_domain"
            )
        except CmdbVarsHost.DoesNotExist:
            raise ImmediateHttpResponse(response=HttpConflict())

        host_vars = get_host_vars(host)

        try:
            wireless_node_domain = host.cmdbvarshost_set.get(
                name="wireless_node_domain"
            )
        except CmdbVarsHost.DoesNotExist:
            wireless_node_domain = CmdbVarsHost(
                host_rel=host, name="wireless_node_domain"
            )

            try:
                community_settings = WifiStackCommunitySettings.objects.get(
                    domain=wireless_node_meshviewer_domain.value
                )
            except WifiStackCommunitySettings.DoesNotExist:
                wireless_node_domain.value = host_vars["wireless_node_domain"]
            else:
                if community_settings.auto_accept:
                    wireless_node_domain.value = wireless_node_meshviewer_domain.value

            wireless_node_domain.save()

        host_vars = get_host_vars(host)

        for variable, value in host_vars.items():
            if variable.startswith("wireless_node_"):
                try:
                    host_var = host.cmdbvarshost_set.get(name=variable)
                except CmdbVarsHost.DoesNotExist:
                    host_var = CmdbVarsHost(host_rel=host, name=variable, value=value)
                    host_var.save(force_insert=True)

        host.refresh_from_db()

        host.os = CmdbOsVersion.objects.get(name="OpenWRT")
        host.host_last_fetch = datetime.now()
        host.save()

        try:
            heartbeat(
                bundle.request, str("h" + str(host.id) + "-" + host.hostname).lower()
            )
        except:
            pass

        return host

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            host = bundle.obj
            host_vars = get_host_vars(host)

            for key, var in host_vars.items():
                if key in self.Meta.public_vars:
                    bundle.data[key] = var

            bundle.data["wireless_node_title"] = host.title

            if host.geoloc is None:
                bundle.data["wireless_node_geo_long"] = 0
                bundle.data["wireless_node_geo_lat"] = 0
            else:
                bundle.data["wireless_node_geo_long"] = host.geoloc["coordinates"][0]
                bundle.data["wireless_node_geo_lat"] = host.geoloc["coordinates"][1]

            # TODO: add final implementation
            # WARNING: POC CODE
            bundle.data["wifi_networks"] = list()

            for site in WifiStackCustomerSite.objects.filter(
                customer__id=host.inst_rel.customer_id
            ):
                for network in site.wifistackcustomerwifinetwork_set.all():
                    bundle.data["wifi_networks"].append(model_to_dict(network))

        return bundle


class GeoJsonDomainResource(Resource):
    class Meta:
        queryset = (
            CmdbVarsHost.objects.filter(
                host_rel__hostname__endswith="nodes.managed-infra.com"
            )
            .filter(name="wireless_node_domain")
            .values("value")
            .distinct()
        )
        resource_name = "wirelessdomain"
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        limit = 0
        max_limit = 0

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        return self.Meta.queryset

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data["domain"] = bundle.obj

        return bundle


class WirelessNodeAdminResource(Resource):
    class Meta:
        queryset = None
        always_return_data = True
        allowed_methods = ["get", "patch"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()
        limit = 0
        max_limit = 0

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        host_id = kwargs["pk"]

        try:
            host = CmdbHost.objects.get(id=host_id)
        except CmdbHost.DoesNotExist:
            raise NotFound

        if host not in nodes_assigned_to_domain_aliases(
            domain_aliases_for_customer(bundle.request.user.id)
        ):
            raise HttpUnauthorized

        return host

    def obj_get_list(self, bundle, **kwargs):
        nodes = nodes_assigned_to_domain_aliases(
            domain_aliases_for_customer(bundle.request.user.id)
        )

        return nodes

    def obj_update(self, bundle, **kwargs):
        host_id = kwargs["pk"]

        try:
            host = CmdbHost.objects.get(id=host_id)
        except CmdbHost.DoesNotExist:
            raise NotFound

        if host not in nodes_assigned_to_domain_aliases(
            domain_aliases_for_customer(bundle.request.user.id)
        ):
            raise HttpUnauthorized

        if "wireless_node_title" in bundle.data:
            host.title = bundle.data["wireless_node_title"]

        if (
            "wireless_node_geo_lat" in bundle.data
            and "wireless_node_geo_long" in bundle.data
        ):
            host.geoloc = {
                "type": "Point",
                "coordinates": [
                    float(bundle.data["wireless_node_geo_long"]),
                    float(bundle.data["wireless_node_geo_lat"]),
                ],
            }

        for variable in WirelessNodeConfigResource.Meta.editable_vars:
            if variable in bundle.data:
                if (
                    variable == "wireless_node_geo_lat"
                    or variable == "wireless_node_geo_long"
                    or variable == "wireless_node_title"
                ):
                    continue

                try:
                    host_var = CmdbVarsHost.objects.get(
                        host_rel_id=host.id, name=variable
                    )
                except CmdbVarsHost.DoesNotExist:
                    host_var = CmdbVarsHost(host_rel_id=host.id, name=variable)

                host_var.value = bundle.data[variable]
                host_var.save()

        host.save()

        return host

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)

        host_vars = get_host_vars(bundle.obj)

        for key, var in host_vars.items():
            if key in WirelessNodeConfigResource.Meta.public_vars:
                bundle.data[key] = var

        bundle.data["wireless_node_title"] = bundle.obj.title

        if bundle.obj.geoloc is None:
            bundle.data["wireless_node_geo_long"] = 0
            bundle.data["wireless_node_geo_lat"] = 0
        else:
            bundle.data["wireless_node_geo_long"] = float(
                bundle.obj.geoloc["coordinates"][0]
            )
            bundle.data["wireless_node_geo_lat"] = float(
                bundle.obj.geoloc["coordinates"][1]
            )

        return bundle


class DomainResource(Resource):
    class Meta:
        queryset = None
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()
        limit = 0
        max_limit = 0

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        domains = domain_aliases_for_customer(bundle.request.user.id)
        return domains

    def dehydrate(self, bundle):
        bundle.data = {"domain": bundle.obj}
        return bundle
