from django.core.management.base import BaseCommand

from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Set domain to values received from meshviewer"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        all_wifi_nodes = CmdbHost.objects.filter(
            hostname__endswith=".nodes.managed-infra.com"
        )

        for node in all_wifi_nodes:
            self.stdout.write("Migrate Node: " + node.hostname)

            try:
                wireless_node_domain = CmdbVarsHost.objects.get(
                    host_rel=node, name="wireless_node_domain"
                )
            except CmdbVarsHost.DoesNotExist:
                wireless_node_domain = CmdbVarsHost(
                    host_rel=node, name="wireless_node_domain", value="fallback"
                )

            try:
                wireless_node_meshviewer_domain = CmdbVarsHost.objects.get(
                    host_rel=node, name="wireless_node_meshviewer_domain"
                )
            except CmdbVarsHost.DoesNotExist:
                wireless_node_meshviewer_domain = CmdbVarsHost(
                    host_rel=node,
                    name="wireless_node_meshviewer_domain",
                    value="fallback",
                )

            wireless_node_domain.value = wireless_node_meshviewer_domain.value

            wireless_node_domain.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
