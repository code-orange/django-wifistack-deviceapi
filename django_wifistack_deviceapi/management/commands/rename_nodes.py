from django.core.management.base import BaseCommand

from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Mass rename all nodes"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        all_wifi_nodes = CmdbHost.objects.filter(
            hostname__endswith=".nodes.managed-infra.com"
        )

        for node in all_wifi_nodes:
            self.stdout.write("Migrate Node: " + node.hostname)

            if not node.hostname.startswith("dc-"):
                node.hostname = "dc-" + node.hostname
                node.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
