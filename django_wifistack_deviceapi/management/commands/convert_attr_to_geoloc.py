from django.core.management.base import BaseCommand

from django_cdstack_deploy.django_cdstack_deploy.func import get_host_vars
from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Convert geographic location to CMDB geolocation field"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        all_wifi_nodes = CmdbHost.objects.filter(
            hostname__endswith=".nodes.managed-infra.com"
        )

        for node in all_wifi_nodes:
            self.stdout.write("Migrate Node: " + node.hostname)
            node_vars = get_host_vars(node)

            node.geoloc = {
                "type": "Point",
                "coordinates": [
                    node_vars["wireless_node_geo_long"],
                    node_vars["wireless_node_geo_lat"],
                ],
            }

            node.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
