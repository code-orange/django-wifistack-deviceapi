from django.db import models

from django_simple_notifier.django_simple_notifier.models import (
    SnotifierEmailContactZammad,
)


class WifiStackMeshviewerUrl(models.Model):
    url = models.CharField(max_length=255, unique=True)
    note = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.note

    class Meta:
        db_table = "wifistack_meshviewer_url"


class WifiStackCommunityInstance(models.Model):
    domain = models.CharField(max_length=255, unique=True)
    instance_id = models.IntegerField()

    class Meta:
        db_table = "wifistack_com_inst"


class WifiStackCommunitySettings(models.Model):
    domain = models.CharField(max_length=255, unique=True)
    auto_accept = models.BooleanField(default=False)

    class Meta:
        db_table = "wifistack_com_settings"
