# Generated by Django 2.2.8 on 2019-12-21 12:22

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_wifistack_deviceapi", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="WifiStackCommunityInstance",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("domain", models.CharField(max_length=255, unique=True)),
                ("instance_id", models.IntegerField()),
            ],
            options={
                "db_table": "wifistack_com_inst",
            },
        ),
    ]
